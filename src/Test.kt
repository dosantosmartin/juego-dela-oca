

import classes.GooseBoard
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import kotlin.test.assertEquals
class BoardShould {
    companion object {
        val goose = GooseBoard()
    }

    @ParameterizedTest
    @ValueSource(strings = ["1","2","3","4","5"])
    fun haveRegularBoxes(position: Int) {
        val movement = goose.getMovement(position)
        assertEquals("Stay in space ${position}", movement)
    }

    @Test
    fun haveBridge() {
        val movement = goose.getMovement(6)
        assertEquals("The Bridge: Go to space 12", movement)
    }

    @ParameterizedTest
    @ValueSource(strings = ["12", "18", "24", "30", "36"])
    fun haveOvertakersEvery6Multiplier(position: Int) {
        val movement = goose.getMovement(position)
        assertEquals("Move two spaces forward.", movement)
    }

    @Test
    fun haveHotel() {
        val movement = goose.getMovement(19)
        assertEquals("The Hotel: Stay for (miss) one turn", movement)
    }

    @Test
    fun haveWell() {
        val movement = goose.getMovement(31)
        assertEquals("The Well: Wait until someone comes to pull you out - they then take your place", movement)
    }

    @Test
    fun haveMaze() {
        val movement = goose.getMovement(42)
        assertEquals("The Maze: Go back to space 39", movement)
    }

    @ParameterizedTest
    @ValueSource(strings = ["50","51","52","53","54","55"])
    fun Test50To55BoxShouldGoToPrison(position: Int) {
        val movement = goose.getMovement(position)
        assertEquals("The Prison: Wait until someone comes to release you - they then take your place", movement)
    }

}
