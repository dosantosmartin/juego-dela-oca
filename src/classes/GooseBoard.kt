package classes

class GooseBoard () {
    val boxes: List<Box> = List<Box>(63) {
        Box("")
    }

    init {
        setBoxesMoveRules()
    }

    fun setBoxesMoveRules() {
        var position = 1
        for (box in boxes) {
            addRegularBoxes(position, box)
            addMoveForwardBoxes(position, box)
            addPrissionBoxes(position, box)
            addBridgeBox(position, box)
            addHotelBox(position, box)
            addWellBox(position, box)
            addMazeBox(position, box)
            position++
        }
    }

    fun getMovement(position: Int): String {
        return boxes[position - 1].move
    }

    fun play() {
        for (boxNumer in 1..63) {
            println(getMovement(boxNumer))
        }
    }

    private fun addRegularBoxes(position: Int, box: Box) {
        box.move = "Stay in space $position"
    }

    private fun addPrissionBoxes(position: Int, box: Box) {
        if (position >= 50 && position <= 55) {
            box.move = "The Prison: Wait until someone comes to release you - they then take your place"
        }
    }

    private fun addMoveForwardBoxes(position: Int, box: Box) {
        if (position != 6 && position % 6 == 0) {
            box.move = "Move two spaces forward."
        }
    }

    private fun addBridgeBox(position: Int, box: Box) {
        when (position) {
            6 -> box.move = "The Bridge: Go to space 12"
        }
    }

    private fun addHotelBox(position: Int, box: Box) {
        when (position) {
            19 -> box.move = "The Hotel: Stay for (miss) one turn"
        }
    }

    private fun addWellBox(position: Int, box: Box) {
        when (position) {
            31 -> box.move = "The Well: Wait until someone comes to pull you out - they then take your place"
        }
    }

    private fun addMazeBox(position: Int, box: Box) {
        when (position) {
            42 -> box.move = "The Maze: Go back to space 39"
        }
    }
}